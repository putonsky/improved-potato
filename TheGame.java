import java.util.Random;
import java.util.Scanner;

public class TheGame {

	public TheGame game = new TheGame();
	

	public static void gameStart() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		

		

		
		
			Random generatedRandomNumber = new Random();
			int randomNumber = generatedRandomNumber.nextInt(100 + 1); // +1 , because (100) bounds declares and int from 0
																		// to 99 and that's what we want to avoid
			

			int j = 5;
			while (j > 0) {
				System.out.print(" Chances left: " + j );
				int userInput = scanner.nextInt();
				if (userInput > 100 || userInput < 1) {

					System.out.println("C'mon, the range is 1 - 100, it's not that hard:)");
					j = j + 1; // this is to keep the 5 chances up; we don't wanna count the out of bounds shot
								// as a missed hit

				}
				if (userInput > randomNumber && userInput <= 100) { // this avoids the if loop run if the userInput
																	// exceeds the limit

					System.out.print("Try a lower number my friend!");
				} else if (userInput < randomNumber && userInput >= 1) {// this avoids the if loop run if the userInput
																		// exceeds the limit
					System.out.print("Try a higher number my friend!");
				} else if (userInput == randomNumber) {
					System.out.print("You win! The mysterious number was " + randomNumber + "!!");
					return;
					
				}
				j--;
				
			}

			System.out.println("\n\nGame over! The mysterious number was: " + randomNumber);
			return;
			
		
	}
}
