import java.util.Scanner;

public class RandomNumberGameApplication {

	public static void main(String[] args) {
		/*
		 * v 1.0 So this is the solution for the assignment I've received from Trevor
		 * Page during Java Minibootcamp The target of this assignment was to create a
		 * game which randomly picks a number from 1 to 100 and asks the player to guess
		 * that number in 5 attempts. If the player tries to type a number out of
		 * bounds, he/she is prompted about it but it shouldn't count as a loss of a hit
		 * chance. This is my interpretation of this assignment, hope you enjoy it, I
		 * did for sure!:)
		 */

		// Author: Wojtek "putonsky" Dere�

		/*
		 * v1.1 Changed the <for> loop usage to <while> one, according to my tutor's
		 * advice. Also changed the name of Scanner variable to more understandable one.
		 * Hope it works well now.
		 */
		
		/*
		 * v1.2 Added the possibility of replaying the game in case the player has won/lost the previous one. 
		 * Some changes to the code in TheGame.gameStart() method, and some code added to main. Gonna work on some 
		 * cleanups now and adding the number of attempts left. I love Java. 
		 * 
		 */
		
		/*
		 * v1.2.1 Added the remaining attempts number to each try. Damn I'm proud: )
		 */
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner (System.in);
		System.out.println("Hello!\n \nI've just generated a random number between 1 and 100, can you guess it? \n");
		String answer = "y";
		while (answer.equals("y")) {
			TheGame.gameStart();
			System.out.println("\n\nWould you like to play again? (y/n)");
			answer = scanner.next();
		}
		System.out.println("Thanks for playing this game, have a good day!");
		
	}

}
		
